
const FIRST_NAME = "Madalina";
const LAST_NAME = "Fota";
const GRUPA = "1077";

/**
 * Make the implementation here
 */

function initCaching() {

    var cache={};
    cache.pageAccessCounter=function(a='home')
    {
        a=a.toLowerCase();
        if(a in this){
            this[a]++;
        }
        else{
            Object.defineProperty(this,a,{
                value:1,
                writable:true
            });
        }
    }
    cache.getCache=function(){
        return this;
    }
    return cache;
}
var x=initCaching();
x.pageAccessCounter('about');
 x.pageAccessCounter('ABOUT');
 x.pageAccessCounter();
 x.pageAccessCounter('HOme');
 x.pageAccessCounter('home');
 x.pageAccessCounter('cONTACT');
console.log(x);

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

